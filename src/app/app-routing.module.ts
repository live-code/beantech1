import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './features/users/users.component';
import { LoginComponent } from './features/login/login.component';


const routes: Routes = [
  { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)},
  { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule)},
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
