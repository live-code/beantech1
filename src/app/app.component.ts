import { Component } from '@angular/core';
import { CounterService } from './shared/services/counter.service';

@Component({
  selector: 'bt-root',
  template: `

    <bt-navbar></bt-navbar>
  
    <router-outlet></router-outlet> 
  `,
  styles: []
})
export class AppComponent {

}
