import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bt-navbar',
  template: `
    <button routerLinkActive="active" routerLink="/home">home</button>
    <button routerLinkActive="active" routerLink="/users">users</button>
    <button routerLinkActive="active" [routerLink]="'/login'">login</button>
    <button routerLinkActive="active" [routerLink]="'/catalog'">catalog</button>
  `,
  styles: [`
    .active {
      background-color: orange;
    }
  `]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
