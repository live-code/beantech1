import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog.component';
import { LastminuteComponent } from './components/lastminute.component';
import { OffersComponent } from './components/offers/offers.component';
import { WarehouseComponent } from './components/warehouse.component';


const routes: Routes = [

  { 
    path: '',
    component: CatalogComponent,
    children: [
      { path: 'offers', loadChildren: () => import('./components/offers/offers.module').then(m => m.OffersModule) },
      { path: 'lastminutes', component: LastminuteComponent },
      { path: 'ware', component: WarehouseComponent },
      { path: '', redirectTo: 'offers' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
