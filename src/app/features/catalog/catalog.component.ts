import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bt-catalog',
  template: `
    
    <hr>
    <button routerLinkActive="active" routerLink="./offers">Offers</button>
    <button routerLinkActive="active" routerLink="./lastminutes">lastminutes</button>
    <button routerLinkActive="active" routerLink="./ware">ware</button>
    
    <br>
    ----
    
    <router-outlet></router-outlet>
    
  `,
  styles: [`
    
    .active { background-color: red }
  
  `]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
