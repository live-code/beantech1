import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { OffersComponent } from './components/offers/offers.component';
import { LastminuteComponent } from './components/lastminute.component';
import { WarehouseComponent } from './components/warehouse.component';


@NgModule({
  declarations: [CatalogComponent, LastminuteComponent, WarehouseComponent],
  imports: [
    CommonModule,
    CatalogRoutingModule
  ]
})
export class CatalogModule { }
