import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './welcome/home.component';
import { HomeGuideComponent } from './guide/home-guide.component';


const routes: Routes = [
  { path: 'guide', component: HomeGuideComponent },
  { path: 'welcome', component: HomeComponent },
  { path: '**', redirectTo: 'welcome' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
