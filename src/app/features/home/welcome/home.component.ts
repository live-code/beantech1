import { Component, OnInit } from '@angular/core';
import { CounterService } from '../../../shared/services/counter.service';

@Component({
  selector: 'bt-home',
  template: `
    <p>
      BENVENUTI IN home !
    </p>
    
    <bt-card></bt-card>

    {{counterService.value}}
    <button (click)="counterService.inc()">+</button>
  `,
  styles: []
})
export class HomeComponent implements OnInit {

  constructor(public counterService: CounterService) {

  }

  ngOnInit(): void {
  }

}
