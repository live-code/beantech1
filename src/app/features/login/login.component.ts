import { Component, OnInit } from '@angular/core';
import { CounterService } from '../../shared/services/counter.service';

@Component({
  selector: 'bt-login',
  template: `
    <p>
      login works!
    </p>


    {{counterService.value}}
    <button (click)="counterService.inc()">+</button>
  `,
  styles: []
})
export class LoginComponent implements OnInit {

  constructor(public counterService: CounterService) {

  }
  ngOnInit(): void {
  }

}
