import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { User } from '../model/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'bt-user-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <form #f="ngForm" (submit)="saveHandler(f)">
      <input 
        type="text" name="name" [ngModel]="active?.name" 
        required
        minlength="3"
        #nameRef="ngModel"
      >
      <button 
        type="submit"
        *ngIf="f.valid">
        {{active?.id ? 'EDIT' : 'ADD'}}
      </button>

      <button
        *ngIf="f.valid"
        type="button" (click)="resetForm.emit()">RESET</button>
    </form>
    
    {{update()}}
  `,
  styles: []
})
export class UserFormComponent implements OnChanges, AfterViewInit {
  @ViewChild('f', { static: true }) form: NgForm;
  @Input() active: User;
  @Input() title: string;
  @Output() resetForm: EventEmitter<void> = new EventEmitter();
  @Output() save: EventEmitter<User> = new EventEmitter();

  constructor(private cd: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.cd.markForCheck();
  }

  ngOnChanges(changes: SimpleChanges) {
    const active: User = changes?.active?.currentValue;
    if (!active) {
      console.log(active)
      this.form.reset();
    }
  }

  saveHandler(f: NgForm) {
    this.save.emit(f.value);
  }

  update() {
    console.log('update form')
  }
}
