import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../model/user';

@Component({
  selector: 'bt-user-list-item',
  template: `
    <li
      (click)="setActive.emit(user)"
      [style.color]="selected ? 'red' : null"
      [style.font-size.px]="selected ? 20 : null"
    >
      {{user.name}}

      <button (click)="deleteHandler(user, $event)">Del</button>
    </li>
  `,
})
export class UserListItemComponent {
  @Input() user: User;
  @Input() selected: boolean;
  @Output() delete: EventEmitter<User> = new EventEmitter()
  @Output() setActive: EventEmitter<User> = new EventEmitter()

  deleteHandler(user: User, event: MouseEvent) {
    event.stopPropagation();
    this.delete.emit(user);
  }

}
