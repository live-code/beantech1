import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../model/user';

@Component({
  selector: 'bt-user-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <bt-user-list-item
      *ngFor="let user of users"
      [user]="user"
      [selected]="user.id === active?.id"
      (delete)="delete.emit($event)"
      (setActive)="setActive.emit($event)"
    ></bt-user-list-item>
  `,
})
export class UserListComponent {
  @Input() users: User[];
  @Input() active: User;
  @Output() delete: EventEmitter<User> = new EventEmitter()
  @Output() setActive: EventEmitter<User> = new EventEmitter()

}
