import { Component, Input, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { UsersStore } from '../services/users.store';

const API = 'http://localhost:3000';

@Component({
  selector: 'bt-users-crud',
  template: `
    
    <bt-user-form
      title="MY LIST"
      [active]="store.active"
      (resetForm)="actions.resetHandler()"
      (save)="actions.saveHandler($event)"
    ></bt-user-form>
    
    <bt-user-list
      [users]="store.users"
      [active]="store.active"
      (delete)="actions.deleteHandler($event)"
      (setActive)="actions.setActiveHandler($event)"
    ></bt-user-list>
  `,
  styles: [],
  providers: [UsersService, UsersStore]

})
export class UsersCrudListComponent implements OnInit  {
  @Input() endpoint: string;

  constructor(public actions: UsersService, public store: UsersStore) {}

  ngOnInit() {
    this.actions.init(this.endpoint);
    const sub = this.actions.getAll()
      .subscribe(res => alert('loaded'))
  }

}
