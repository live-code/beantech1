
export interface User {
  id: number;
  name: string;
  username: string;
}

export type FormUser = Omit<User, 'id'>;
