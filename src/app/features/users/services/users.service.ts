import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { UsersStore } from './users.store';
import { share } from 'rxjs/operators';

const API = 'http://localhost:3000';

@Injectable()
export class UsersService {

  endpoint: string;

  constructor(private http: HttpClient, private store: UsersStore) {
    console.log('ctrs');
  }

  init(endpoint: string) {
    this.endpoint = endpoint;
  }
  getAll() {
    const subject$: Subject<User[]> = new Subject();
    subject$.subscribe(res => this.store.users = res)

    const req$ = this.http.get<User[]>(`${API}/${this.endpoint}`)
    req$.subscribe(subject$);

    return subject$;
  }

  deleteHandler(user: User) {
    this.http.delete(`${API}/${this.endpoint}/${user.id}`)
      .subscribe(() => {
        this.store.users = this.store.users.filter(u => u.id !== user.id);
      });
  }

  saveHandler(user: User) {
    if (this.store.active?.id) {
      this.edit(user);
    } else {
      this.add(user);
    }
  }

  add(user: User) {
    this.http.post<User>(`${API}/${this.endpoint}/`, user)
      .subscribe(res => {
        this.store.users = [...this.store.users, res];
        this.resetHandler();
      });

  }

  edit(user: User) {
    this.http.patch<User>(`${API}/${this.endpoint}/${this.store.active.id}`, user)
      .subscribe(res => {
        this.store.users = this.store.users.map(u => u.id === this.store.active.id ? res : u)
      });
  }

  setActiveHandler(user: User) {
    this.store.active = user;
  }


  resetHandler() {
    this.store.active = {}  as User;
  }
}
