import { User } from '../model/user';

export class UsersStore {
  users: Array<User>;
  active: User = {} as User;;
}
