import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './model/user';
import { UsersService } from './services/users.service';

const API = 'http://localhost:3000';

@Component({
  selector: 'bt-users',
  template: `

    <bt-users-crud endpoint="users"></bt-users-crud>
  `,
  styles: [],
})
export class UsersComponent {



}
