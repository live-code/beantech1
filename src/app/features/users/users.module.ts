import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFormComponent } from './components/user-form.component';
import { UserListComponent } from './components/user-list.component';
import { UserListItemComponent } from './components/user-list-item.component';
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';
import { RouterModule } from '@angular/router';
import { UsersService } from './services/users.service';
import { UsersCrudListComponent } from './components/users-crud-list.component';

@NgModule({
  declarations: [
    UsersComponent,
    UsersCrudListComponent,
      UserFormComponent,
      UserListComponent,
      UserListItemComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {path: '', component: UsersComponent}
    ])
  ],
  exports: [
    UsersComponent
  ],

})
export class UsersModule { }
