import { Injectable } from '@angular/core';

@Injectable()
export class CounterService {

  value = 0;

  inc() {
    this.value = this.value + 1;
  }
}
