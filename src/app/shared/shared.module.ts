import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterService } from './services/counter.service';
import { CardComponent } from './components/card.component';


export const COMPONENTS = [CardComponent];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [
    CommonModule
  ],

})
export class SharedModule {

  static forRoot(): ModuleWithProviders<SharedModule> {
    return  {
      ngModule: SharedModule,
      providers: [ CounterService ]
    };
  }
}
